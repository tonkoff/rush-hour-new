﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RushHour.Entities;
using RushHour.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;

        public ProfileController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var userId = _userManager.GetUserId(HttpContext.User);

            if (userId == null)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                ApplicationUser user = await _userManager.FindByIdAsync(userId);
                return View(user);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Index(ApplicationUser userdetails)
        {
            var userId = _userManager.GetUserId(HttpContext.User);

            ApplicationUser user = await _userManager.FindByIdAsync(userId);

            user.Email = userdetails.Email;
            user.Name = userdetails.Name;
            user.PhoneNumber = userdetails.PhoneNumber;

            IdentityResult UpdateUserResult = await _userManager.UpdateAsync(user);

            var resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);

            var result = await _userManager.ResetPasswordAsync(user, resetToken, userdetails.PasswordHash);

            if (UpdateUserResult.Succeeded)
            {
                return RedirectToAction("Index", "Appointment");
            }

            return View(userdetails);
        }
    }
}