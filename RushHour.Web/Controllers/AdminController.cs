﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RushHour.Entities;
using RushHour.Web.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RushHour.Web.Models.AdminViewModels;
using Microsoft.AspNetCore.Authorization;
using RushHour.Services;
using RushHour.Web.Models.AppoinmentViewModels;

namespace RushHour.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private IAppointmentService _service;

        public AdminController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            IAppointmentService service)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _service = service;
        }

        public IActionResult Index()
        {
            AdminListViewModel model = new AdminListViewModel()
            {
                ApplicationUsers = _userManager.Users.ToList()
            };

            return View(model);
        }

        public IActionResult Appointments()
        {
            return View();
        }

        public JsonResult GetEvents()
        {
            AppointmentListViewModel model = new AppointmentListViewModel
            {
                Appointments = _service.GetAppointments()
            };

            return new JsonResult(model.Appointments);
        }

        public async Task<IActionResult> Delete(string id)
        {
            ApplicationUser applicationUser = await _userManager.FindByIdAsync(id);

            if (applicationUser != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(applicationUser);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                ModelState.AddModelError("", "User Not Found");
            }

            return View("Index");
        }

        public async Task<IActionResult> Edit(string Id)
        {
            if (Id == null)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ApplicationUser user = await _userManager.FindByIdAsync(Id);
                return View(user);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ApplicationUser userdetails, string Id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(Id);

            user.Email = userdetails.Email;
            user.Name = userdetails.Name;
            user.PhoneNumber = userdetails.PhoneNumber;

            IdentityResult updateUserResult = await _userManager.UpdateAsync(user);

            var resetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
            var password = await _userManager.ResetPasswordAsync(user, resetToken, userdetails.PasswordHash);

            if (updateUserResult.Succeeded)
            {
                return RedirectToAction("Index");
            }

            return View(userdetails);
        }
    }
}