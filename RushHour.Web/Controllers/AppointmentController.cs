﻿using Microsoft.AspNetCore.Mvc;
using RushHour.Entities;
using RushHour.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RushHour.Web.Models.AppoinmentViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace RushHour.Web.Controllers
{
    [Authorize]
    public class AppointmentController : Controller
    {
        private IAppointmentService service;

        private IDataService<Activity> activityService;

        private readonly UserManager<ApplicationUser> _userManager;

        public AppointmentController(IAppointmentService service,
                                     IDataService<Activity> activityService,
                                     UserManager<ApplicationUser> userManager)
        {
            this.service = service;
            this.activityService = activityService;
            this._userManager = userManager;
        }

        public IActionResult Index()
        {
            ApplicationUser user = _userManager.GetUserAsync(HttpContext.User).Result;

            AppointmentListViewModel model = new AppointmentListViewModel()
            {
                Appointments = service.GetCurrentUserAppointments(user.Id)
            };

            return View(model);
        }

        public JsonResult GetEvents()
        {
            ApplicationUser user = _userManager.GetUserAsync(HttpContext.User).Result;

            AppointmentListViewModel model = new AppointmentListViewModel
            {
                Appointments = service.GetCurrentUserAppointments(user.Id)
            };

            return new JsonResult(model.Appointments);
        }

        [HttpGet]
        public IActionResult Create()
        {
            AppointmentCreateViewModel model = new AppointmentCreateViewModel
            {
                StartDateTime = DateTime.Now,

                Activity = activityService.GetAll()
            };

            return View(model);
        }

        [HttpPost]
        public IActionResult Create(AppointmentCreateViewModel model)
        {
            ApplicationUser user = _userManager.GetUserAsync(HttpContext.User).Result;

            if (ModelState.IsValid)
            {
                Appointment appointment = new Appointment();

                Activity activity = new Activity();

                activity = activityService.Get(model.ActivityId);

                appointment.StartDateTime = model.StartDateTime;
                appointment.EndDateTime = model.StartDateTime.AddHours(activity.Duration);
                appointment.ApplicationUserId = user.Id;
                appointment.ActivityId = model.ActivityId;

                service.Insert(appointment);

                return RedirectToAction("Index", "Appointment");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Appointment appointment = service.Get(id);

            service.Delete(appointment);

            return RedirectToAction("Index", "Appointment");
        }

        [HttpGet]
        public IActionResult Edit(Guid id)
        {
            if (id == Guid.Empty)
            {
                return NotFound();
            }

            //var items = AutoMapper.Mapper.Map<Appointment, AppointmentEditViewModel>(service.Get(id));

            Appointment appointment = new Appointment();

            appointment = service.Get(id);

            AppointmentEditViewModel appointmentEditViewModel = new AppointmentEditViewModel
            {
                Id = appointment.Id,
                ActivityId = appointment.ActivityId,
                StartDateTime = appointment.StartDateTime,

                Activity = activityService.GetAll(),
                ApplicationUserId = appointment.ApplicationUserId
            };

            return View(appointmentEditViewModel);
        }

        [HttpPost]
        public IActionResult Edit(AppointmentEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                // var item = Mapper.Map<AppointmentListViewModel, Appointment>(model);
                ApplicationUser user = _userManager.GetUserAsync(HttpContext.User).Result;

                Appointment appointment = new Appointment();
                Activity activity = new Activity();
                activity = activityService.Get(model.ActivityId);

                appointment.Id = model.Id;
                appointment.ActivityId = model.ActivityId;
                appointment.StartDateTime = model.StartDateTime;
                appointment.EndDateTime = model.StartDateTime.AddHours(activity.Duration);
                appointment.ApplicationUserId = user.Id;

                service.Update(appointment);

                return RedirectToAction("Index", "Appointment");
            }

            return View(model);
        }
    }
}