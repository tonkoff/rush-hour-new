﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.AdminViewModels
{
    public class AdminListViewModel
    {
        public List<RushHour.Entities.ApplicationUser> ApplicationUsers { get; set; }
    }
}