﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.AppoinmentViewModels
{
    public class AppointmentListViewModel
    {
        public List<RushHour.Entities.AppointmentActivity> Appointments { get; set; }
    }
}