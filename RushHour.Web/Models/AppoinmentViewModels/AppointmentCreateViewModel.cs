﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RushHour.Web.Models.AppoinmentViewModels
{
    public class AppointmentCreateViewModel
    {
        public Guid Id { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public Guid ApplicationUserId { get; set; }
        public Guid ActivityId { get; set; }

        public virtual List<RushHour.Entities.Activity> Activity { get; set; }
    }
}