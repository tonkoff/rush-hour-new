﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Entities
{
    public class AppointmentActivity : BaseEntity
    {
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string ApplicationUserId { get; set; }

        public Guid ActivityId { get; set; }

        public string ActivityName { get; set; }
        public float ActivityDuration { get; set; }
        public decimal ActivityPrice { get; set; }
        public string UserName { get; set; }
    }
}