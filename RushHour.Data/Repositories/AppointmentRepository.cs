﻿using Microsoft.EntityFrameworkCore;
using RushHour.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RushHour.Data.Repositories
{
    public class AppointmentRepository : BaseRepository<Appointment>, IAppointmentRepository
    {
        public AppointmentRepository(ApplicationDbContext db) : base(db)
        {
        }

        public IQueryable<AppointmentActivity> GetAppointments()
        {
            // var result = db.Appointments.Include(a => a.Activity).Where(a => Contex.currentUser.Id == a.UserId);

            var result = from ap in db.Appointments
                         join ac in db.Activities on ap.ActivityId equals ac.Id
                         join us in db.Users on ap.ApplicationUserId equals us.Id

                         select new AppointmentActivity
                         {
                             Id = ap.Id,
                             StartDateTime = ap.StartDateTime,
                             EndDateTime = ap.EndDateTime,
                             //  ApplicationUserId = ap.ApplicationUserId,
                             UserName = us.Name,
                             ActivityName = ac.Name,
                             ActivityDuration = ac.Duration,
                             ActivityPrice = ac.Price
                         };

            return result;
        }

        public IQueryable<AppointmentActivity> GetCurrentUserAppointments(string Idd)
        {
            // var result = db.Appointments.Include(a => a.Activity).Where(a => Contex.currentUser.Id == a.UserId);

            var result = from ap in db.Appointments
                         join ac in db.Activities on ap.ActivityId equals ac.Id
                         where ap.ApplicationUserId == Idd

                         select new AppointmentActivity
                         {
                             Id = ap.Id,
                             StartDateTime = ap.StartDateTime,
                             EndDateTime = ap.EndDateTime,
                             // ApplicationUserId = ap.ApplicationUserId,
                             //   UserName = us.Name,
                             ActivityName = ac.Name,
                             ActivityDuration = ac.Duration,
                             ActivityPrice = ac.Price
                         };

            return result;
        }
    }
}