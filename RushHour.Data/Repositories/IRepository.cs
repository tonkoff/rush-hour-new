﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RushHour.Entities;

namespace RushHour.Data.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();

        T Get(Guid Id);

        void Insert(T entity);

        void Update(T entity);

        void Delete(T entity);

        // IQueryable<Appointment> GetAllAppointments();
    }
}