﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RushHour.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace RushHour.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    // Change the name of the table to be Users instead of AspNetUsers
        //    // modelBuilder.Entity<User>().ToTable("AspNetUsers");
        //}

        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<Activity> Activities { get; set; }
    }
}