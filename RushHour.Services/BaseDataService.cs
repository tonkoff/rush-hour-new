﻿using System;
using System.Collections.Generic;
using System.Text;
using RushHour.Entities;
using RushHour.Data.Repositories;
using System.Linq;
using System.Linq.Expressions;

namespace RushHour.Services

{
    public class BaseDataService<T> : IDataService<T> where T : BaseEntity
    {
        protected IRepository<T> repository;

        public BaseDataService(IRepository<T> repository)
        {
            this.repository = repository;
        }

        public void Delete(T entity)
        {
            repository.Delete(entity);
        }

        public T Get(Guid Id)
        {
            return repository.Get(Id);
        }

        public T Get(Expression<Func<T, bool>> filter)
        {
            return repository.GetAll().SingleOrDefault(filter);
        }

        public List<T> GetAll()
        {
            return repository.GetAll().ToList();
        }

        //public List<Appointment> GetAllAppointments()
        //{
        //    return repository.GetAllAppointments().ToList();
        //}

        public List<T> GetAll(Expression<Func<T, bool>> filter, int itemsCount)
        {
            return repository.GetAll()
                 .Where(filter)
                 .Take(itemsCount)
                 .ToList();
        }

        public List<T> GetlAll(Expression<Func<T, bool>> filter)
        {
            return repository.GetAll()
                 .Where(filter)
                 .ToList();
        }

        public void Insert(T entity)
        {
            repository.Insert(entity);
        }

        public void Update(T entity)
        {
            repository.Update(entity);
        }
    }
}