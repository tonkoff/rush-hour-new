﻿using System;
using System.Collections.Generic;
using System.Text;
using RushHour.Entities;
using System.Linq.Expressions;

namespace RushHour.Services
{
    public interface IDataService<T> where T : BaseEntity
    {
        List<T> GetAll();

        List<T> GetlAll(Expression<Func<T, bool>> filter);

        List<T> GetAll(Expression<Func<T, bool>> filter, int itemsCount);

        T Get(Guid Id);

        T Get(Expression<Func<T, bool>> filter);

        void Insert(T entity);

        void Update(T entity);

        void Delete(T entity);

        // List<Appointment> GetAllAppointments();
    }
}